///cycle()
/*
    nnn or addr - A 12-bit value, the lowest 12 bits of the instruction
    n or nibble - A 4-bit value, the lowest 4 bits of the instruction
    x - A 4-bit value, the lower 4 bits of the high byte of the instruction
    y - A 4-bit value, the upper 4 bits of the low byte of the instruction
    kk or byte - An 8-bit value, the lowest 8 bits of the instruction
*/
with(obj_chip8)
{
    opcode = (memory[pc] << 8) | (memory[pc + 1]);// Make an explanation for this

    nnn = (opcode & $0FFF);
    kk  = (opcode & $00FF);
    X   = (opcode & $0F00) >> 8;
    Y   = (opcode & $00F0) >> 4;
    n   = (opcode & $000F);
    
    scr_update_keys()//Update Pressed Keys
    scr_overflow()//Checks if registers are above their highest values
    switch(opcode & $F000)
    {
        case $0000:
            switch(opcode & $00FF)
            {   
                case $00E0:// Clear Screen
                    for(yy=0;yy<32;yy++)
                    {
                        for(xx=0;xx<64;xx++)
                        {
                            gfx[xx,yy] = 0;
                        }
                    }
                    pc +=2;
                    break;
                case $00EE://If it isn't clear screen, has to be return from subroutine
                    --sp;
                    pc = stack[sp];
                    pc += 2;
                break;
            }
        break;
        
        case $1000://1NNN Jump to NNN
        pc = nnn;
        break;
        
        case $2000://2nnn Call subroutine at nnn.
        stack[sp] = pc;
        sp++;
        pc = nnn;
        break;
        
        case $3000://3xkk Skip next instruction if Vx = kk.
        if (V[X] = kk)
            pc +=4;
        else
            pc +=2;
        break;
        
        case $4000://4xkk Skip next instruction if Vx != kk.
        if (V[X] != kk)
            pc +=4;
        else
            pc +=2;
        break;
        
        case $5000://5xy0 Skip next instruction if Vx = Vy.
        if (V[X] = V[Y])
            pc +=4;
        else
            pc +=2;
        break;
        
        case $6000://6xkk Set Vx = kk.
        V[X] = kk;
        pc += 2;
        break;
        
        case $7000://7xkk Set Vx = Vx + kk
        V[X] += kk;
        pc += 2;
        break;
        
        case $8000:
            switch(opcode & $000F)
            {
                case 0://8xy0 - LD Vx, Vy
                V[X] = V[Y]
                pc += 2;
                break;
                
                case 1://8xy1 - OR Vx, Vy
                V[X] |= V[Y]
                pc += 2;
                break;
                
                case 2://8xy2 - AND Vx, Vy
                V[X] &= V[Y]
                pc += 2;
                break;
                
                case 3://8xy3 - XOR Vx, Vy
                V[X] ^= V[Y]
                pc += 2;
                break;
                
                case 4://8xy4 - ADD Vx, Vy
                if (V[X]>255)
                    V[$F]=1;
                V[X] += V[Y]
                pc += 2;
                break;
                
                case 5://8xy5 - SUB Vx, Vy
                if (V[Y] > V[X])
                    V[$F]=1;
                V[X] -= V[Y]
                pc += 2;
                break;
                
                case 6://Set Vx = Vx SHR 1
                V[$F] = (V[X] & $1);
                V[X] = (V[X] >> 1);
                pc += 2;
                break;
                
                case 7://8xy7 - SUB Vy, Vx
                if (V[X] > V[Y])
                    V[$F]=1;
                V[X] = (V[Y] - V[X])
                pc += 2;
                break;
                
                case $E://Set Vx = Vx SHL 1
                V[$F] = V[X] >> 7;
                V[X] = (V[X] << 1);
                pc += 2;
                break;
                
                default:
                //show_message("unknown opcode: "+string(opcode))
                //pc += 2;// I guess we'll try to salvage the situation hehe
                break;
            }    
        break;
            
        case $9000://Skip next instruction if Vx != Vy
        if V[X]!=V[Y]
            pc += 4
        else
            pc += 2
        break;
        
        case $A000://The value of register I is set to nnn
        I = nnn;
        pc += 2;
        break;
        
        case $B000://The program counter is set to nnn plus the value of V0
        pc = (nnn + V[0])
        break;
        
        case $C000://Set Vx = random byte AND kk
        V[X] = (round(random(255)) & kk);
        pc += 2;
        break;
        
        case $D000://Display n-byte sprite starting at memory location I at (Vx, Vy), set VF = collision
        var spr_height, spr_pixel;
        spr_height = n;
        for(y_line = 0; y_line < spr_height; y_line++)
        {
            spr_pixel = memory[I + y_line];
            for(x_line = 0; x_line < 8; x_line++)
            {
                if((spr_pixel & ($80 >> x_line)) != 0)
                {
                    if(gfx[(V[X] + x_line) mod 64, (V[Y] + y_line) mod 32] = 1)
                    {
                        V[$F] = 1;//Let's the Program know a collision occured
                    }
                    gfx[(V[X] + x_line) mod 64, (V[Y] + y_line) mod 32] ^= 1;
                }
            }
        }
        pc += 2;
        break;
        
        case $E000:
        switch(opcode & $00FF)
        {
            case $009E://Skip next instruction if key with the value of Vx is pressed
            if(key[V[X]] = 1)
                pc += 4;
            else
                pc += 2;
            break;
        
            case $00A1://Skip next instruction if key with the value of Vx is not pressed
            if(key[V[X]] != 1)
                pc += 4
            else
                pc += 2;
            break;
            
            default:
            show_message("Unknown")
            break;
        }
        break;
        
        case $F000:
            switch(opcode & $00FF)
            {
                case $0007://Set Vx = delay timer value.
                V[X] = delay_timer;
                pc += 2;
                break;
                
                case $000A://Wait for a key press, store the value of the key in Vx
                for(i=0; i<16; i++)
                {
                    if (key[i] = 1)
                    {
                        V[X] = i;
                        pc += 2;
                        break;
                    }
                }
                break;
                
                case $0015://Set delay timer = Vx
                delay_timer = V[X];
                pc += 2;
                break;
            
                case $0018://Set sound timer = Vx
                sound_timer = V[X];
                pc += 2;
                break;
                
                case $001E://Set I = I + Vx
                I = I + V[X];
                pc += 2;
                break;
                
                case $0029://Set I = location of sprite for digit Vx
                I = V[X];
                pc += 2;
                break;
                
                case $0033://Store BCD representation of Vx in locations I, I+1, and I+2
                memory[I]     =  V[X] / 100;
                memory[I + 1] = (V[X] / 10) div 10;
                memory[I + 2] = (V[X] div 100) div 10;
                pc += 2;
                break;
                
                case $0055://Store registers V0 through Vx in memory starting at location I
                for(i=0;i<=X;i++)
                {
                    memory[I + i] = V[i];
                }
                pc += 2;
                break;
                
                case $0065://Read registers V0 through Vx from memory starting at location I
                for(i=0;i<=X;i++)
                {
                    V[i] = memory[I + i];
                }
                pc += 2;
                break;
            }
        break;
        
        default:
        show_message("unknown opcode: "+string(dec_to_hex(opcode & $F000)))
        //pc += 2;// I guess we'll try to salvage the situation hehe
        break;
    }
}
