///Scr_overflow
with(obj_chip8)
{
    for(i=0;i<16;i++)
    {
        if(V[i]>255)
        {
            V[i] = V[i] mod 256;
        }
    }
}
