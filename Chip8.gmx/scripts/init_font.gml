with(obj_chip8)
{
    //Small Font
    chip8_fontset[1]=$F0; chip8_fontset[2]=$90; chip8_fontset[3]=$90; chip8_fontset[4]=$90; chip8_fontset[5]=$F0; //0
    chip8_fontset[6]=$20; chip8_fontset[7]=$60; chip8_fontset[8]=$20; chip8_fontset[9]=$20; chip8_fontset[10]=$70; //1
    chip8_fontset[11]=$F0; chip8_fontset[12]=$10; chip8_fontset[13]=$F0; chip8_fontset[14]=$80; chip8_fontset[15]=$F0; //2
    chip8_fontset[16]=$F0; chip8_fontset[17]=$10; chip8_fontset[18]=$F0; chip8_fontset[19]=$10; chip8_fontset[20]=$F0; //3
    chip8_fontset[21]=$90; chip8_fontset[22]=$90; chip8_fontset[23]=$F0; chip8_fontset[24]=$10; chip8_fontset[25]=$10; //4
    chip8_fontset[26]=$F0; chip8_fontset[27]=$80; chip8_fontset[28]=$F0; chip8_fontset[29]=$10; chip8_fontset[30]=$F0; //5
    chip8_fontset[31]=$F0; chip8_fontset[32]=$80; chip8_fontset[33]=$F0; chip8_fontset[34]=$90; chip8_fontset[35]=$F0; //6
    chip8_fontset[36]=$F0; chip8_fontset[37]=$10; chip8_fontset[38]=$20; chip8_fontset[39]=$40; chip8_fontset[40]=$40; //7
    chip8_fontset[41]=$F0; chip8_fontset[42]=$90; chip8_fontset[43]=$F0; chip8_fontset[44]=$90; chip8_fontset[45]=$F0; //8
    chip8_fontset[46]=$F0; chip8_fontset[47]=$90; chip8_fontset[48]=$F0; chip8_fontset[49]=$10; chip8_fontset[50]=$F0; //9
    chip8_fontset[51]=$F0; chip8_fontset[52]=$90; chip8_fontset[53]=$F0; chip8_fontset[54]=$90; chip8_fontset[55]=$90; //A
    chip8_fontset[56]=$E0; chip8_fontset[57]=$90; chip8_fontset[58]=$E0; chip8_fontset[59]=$90; chip8_fontset[60]=$E0; //B
    chip8_fontset[61]=$F0; chip8_fontset[62]=$80; chip8_fontset[63]=$80; chip8_fontset[64]=$80; chip8_fontset[65]=$F0; //C
    chip8_fontset[66]=$E0; chip8_fontset[67]=$90; chip8_fontset[68]=$90; chip8_fontset[69]=$90; chip8_fontset[70]=$E0; //D
    chip8_fontset[71]=$F0; chip8_fontset[72]=$80; chip8_fontset[73]=$F0; chip8_fontset[74]=$80; chip8_fontset[75]=$F0; //E
    chip8_fontset[76]=$F0; chip8_fontset[77]=$80; chip8_fontset[78]=$F0; chip8_fontset[79]=$80; chip8_fontset[80]=$80; //F
    //Load Into Ram
    for(i=81;i<=160;i++)
    {
        memory[i-1]=chip8_fontset[i-80];
    }


}
