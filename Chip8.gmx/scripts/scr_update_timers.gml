///scr_update_timers()
with(obj_chip8)
{
    if delay_timer > 0
        delay_timer--;
    if sound_timer > 0
        sound_timer--;
}
