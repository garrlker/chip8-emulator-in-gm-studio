with(obj_chip8)
{
    var fname,file;
    fname = get_open_filename("*.ch8","")
    file = file_bin_open(fname,0)
    for(i=0;i<file_bin_size(file);i++)
    {
        memory[pc + i] = file_bin_read_byte(file)
    }
    if(file_bin_size(file) < 1)
    {
        show_message("Not Loaded:(")
    }
    file_bin_close(file)
}
    
